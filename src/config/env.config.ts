import 'dotenv/config';

export const ENV = {
    PORT: process.env.PORT,

    DB_NAME:process.env.DB_NAME,
    DB_HOST: process.env.DB_HOST,
    DB_USER: process.env.DB_USER,
    DB_PASSWORD: process.env.DB_PASSWORD,
    DB_AUTH_SOURCE: process.env.DB_AUTH_SOURCE,
}