import { Injectable } from '@nestjs/common';
import { MongoClient } from 'mongodb';
import { ENV } from 'src/config/env.config';
import { ProductDto } from './dto/product.dto';

const url = `mongodb://${ENV.DB_USER}:${ENV.DB_PASSWORD}@${ENV.DB_HOST}/?authSource=${ENV.DB_AUTH_SOURCE}`
const client = new MongoClient(url);
const dbName = ENV.DB_NAME;

@Injectable()
export class ProductsService {

    async searchProducts(){
        await client.connect();
        const db = client.db(dbName);
        const collection = db.collection('products');

        const findResult = await collection.find({}).toArray();
        return findResult
    }

    async saveProduct(productDto: ProductDto){
        await client.connect();
        const db = client.db(dbName);
        const collection = db.collection('products');

        const result = await collection.insertOne(productDto)
        return result
    }
}
