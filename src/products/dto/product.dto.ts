import { Type } from "class-transformer";
import { IsArray, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import { ObjectId } from "mongodb";

export class ProductDto {
    @IsNotEmpty()
    @Type(() => ObjectId)
    _id: ObjectId;

    @IsString()
    @IsNotEmpty()
    readonly SKU: string;

    @IsString()
    @IsOptional()
    readonly code: string;

    @IsString()
    @IsNotEmpty()
    readonly name: string;

    @IsString()
    @IsOptional()
    readonly description: string;

    @IsArray()
    @IsNotEmpty()
    readonly pictures: string[];

    @IsNumber()
    @IsNotEmpty()
    readonly price: number;

    @IsString()
    @IsNotEmpty()
    readonly currency: string;
}