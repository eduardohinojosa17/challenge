import { Body, Controller, Get, HttpStatus, Post, Response, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ProductDto } from './dto/product.dto';
import { ProductsService } from './products.service';

@Controller('products')
export class ProductsController {
    constructor(
        private productsService: ProductsService
    ) { }

    @Get('search')
    @UseGuards(AuthGuard('jwt'))
    public async getProducts(@Response() res) {
        try {
            const products = await this.productsService.searchProducts()
            if (products) {
                return res.status(HttpStatus.OK).json({ message: 'Products returned correctly', products })
            }
            return res.status(HttpStatus.NOT_FOUND).json({ message: 'The resource was not found' })
        } catch (error) {
            console.log('Error performing the query: ', error);
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'An error occurred', error });
        }
    }
    @Post('create')
    @UseGuards(AuthGuard('jwt'))
    public async createProduct(@Response() res, productDto: ProductDto) {
        try {
            const product = await this.productsService.saveProduct(productDto)
            return res.status(HttpStatus.OK).json({ message: 'Product created successfully', product });
        } catch (error) {
            console.log('Error performing the query: ', error);
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'An error occurred', error });
        }
    }
}
