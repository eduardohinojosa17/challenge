import { IsEmail, IsNotEmpty, IsString } from "class-validator";

export class LoginDto {
    @IsString()
    @IsNotEmpty()
    readonly email_username: string;

    @IsString()
    @IsNotEmpty()
    readonly password: string;
}