import { Body, Controller, Get, HttpStatus, Post, Response } from '@nestjs/common';
import { LoginDto } from './dto/login.dto';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
    constructor(
        private authService: AuthService
    ) { }

    @Post('login')
    public async login(@Response() res, @Body() login: LoginDto) {
        try {
            const token = await this.authService.loginUser(login)
            if (token) {
                return res.status(HttpStatus.OK).json({ message: 'Successful account access', token })
            }
            return res.status(HttpStatus.NOT_FOUND).json({ message: 'User, password incorrect or rol-state not admitted ' })
        } catch (error) {
            console.log('Error performing the query: ', error);
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: 'An error occurred', error });
        }
    }

    
}
