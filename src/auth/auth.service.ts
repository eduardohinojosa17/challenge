import { Injectable } from '@nestjs/common';
import { LoginDto } from './dto/login.dto';
import { MongoClient } from 'mongodb';
import { sign } from 'jsonwebtoken'
import { sha256 } from 'js-sha256';
import { ENV } from 'src/config/env.config';

const url = `mongodb://${ENV.DB_USER}:${ENV.DB_PASSWORD}@${ENV.DB_HOST}/?authSource=${ENV.DB_AUTH_SOURCE}`
const client = new MongoClient(url);
const dbName = ENV.DB_NAME;


@Injectable()
export class AuthService {
    constructor(
    ) { }

    async loginUser(login: LoginDto) {
        await client.connect();
        const db = client.db(dbName);
        const collection = db.collection('users');
        const user = await collection.findOne({ $or: [{ email: login.email_username }, { username: login.email_username }] });

        if (user.password == sha256(login.password) && user.role == 'admin' && user.active == true) {
            const expiresIn = 15552000
            const accessToken = sign(
                {
                    id: user.id,
                    email: user.email,
                    role: user.role
                },
                'Churrasco',
                { expiresIn }
            )
            return { accessToken, expiresIn }
        }
        return null
    }

    public async validateAuthToken(email: string) {
        await client.connect();
        const db = client.db(dbName);
        const collection = db.collection('users');
        return await collection.findOne({ email });
    }

}
